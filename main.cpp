#include "main.h"
#include <ctime>

inline bool mfileExists(const std::string name) {
    std::ifstream f(name.c_str());
    return f.good();
}

void writeObs(const Boson &sites, const MPS &psi, int sweepNum){
    auto H = hamiltonian(sites);
    auto Ell = obsEll(sites);
    auto N = obsN(sites);
    auto N_1xM = obsN_1xM(sites);
    auto NcNi = obsNN(sites);

    std::cout << "Sweep=\t" << sweepNum << "\t";
    std::cout << "rtime=\t" << clock()/CLOCKS_PER_SEC << "\t";
    std::cout << "vmemkB=\t" << process_mem_usage() << "\t";
    std::cout << "vmemPeakkB=\t" << process_peak_mem_usage() << "\t";
    std::cout << "dim=\t" << maxLinkDim(psi) << "\t";

    std::cout << "H=\t" <<calc(H,psi) << "\t";
    std::cout << "Ell=\t" <<calc(Ell,psi) << "\t";
    std::cout << "N=\t" <<calc(N,psi) << "\t";

    std::cout << "Ni=\t";
    for(auto &ni : N_1xM){
        std::cout << calc(ni,psi) << "\t";
    }

    std::cout << "NcNi=\t";
    for(auto &ncni : NcNi){
        std::cout << calc(ncni,psi) << "\t";
    }
    std::cout << std::endl;
}

void saveIfNeeded(Boson &sites, MPS &psi,int sweepNum,Args const& args = Args::global()){
    if(sweepNum%args.getInt("nSweepsSave")==(args.getInt("nSweepsSave")-1)){
        writeToFile(args.getString("name")+"_sites",sites);
        writeToFile(args.getString("name")+"_psi",psi);
    }
}

Boson loadSitesOrCreateNew(Args const& args = Args::global()){
    if( args.getString("startingPsi")!="none"){
        Boson sites;
        readFromFile( args.getString("startingPsi")+"_sites",sites);
        return sites;
    }
    return Boson(args.getInt("M"));
}


MPS loadPsiOrCreateNew(Boson sites,Args const& args = Args::global()){
    if( args.getString("startingPsi")!="none"){
        MPS psi(sites);
        readFromFile( args.getString("startingPsi")+"_psi" ,psi);
        return psi;
    }
    return genPsi0(sites);
}


void calculate(Args const& args = Args::global()){
    int totalSweepsNum = 0;
    Boson sites = loadSitesOrCreateNew();
    MPS psi = loadPsiOrCreateNew(sites);
    MPO H = hamiltonian(sites);
    for(int stage=0; stage<argLength("sweepsInStage"); stage++){
        std::cout << args.getInt("minDim_"+std::to_string(stage)) << " "
                  <<args.getInt("maxDim_"+std::to_string(stage)) << " "
                 <<args.getReal("cutoff_"+std::to_string(stage)) << std::endl;
        auto sweep = Sweeps(1,args.getInt("minDim_"+std::to_string(stage)),
                              args.getInt("maxDim_"+std::to_string(stage)),
                              args.getReal("cutoff_"+std::to_string(stage)));
        for(int sweepNum=0;sweepNum<args.getInt("sweepsInStage_"+std::to_string(stage)); sweepNum++){
            dmrg(psi,H,sweep,{"Quiet=",true,"Silent=",true});
            writeObs(sites,psi,totalSweepsNum);
            saveIfNeeded(sites,psi,totalSweepsNum);
            totalSweepsNum++;
        }
    }
}



int main(int argc, char *argv[]){
    addArgs(argc,argv);

    addArg("M",24);
    addArg("N",4);
    addArg("PBC",true);

    addArg("g",0.0);
    addArg("gnn",0.0);

    addArg("ConserveQNs",true);
    addArg("ConserveNb",true);
    addArg("MaxOcc",2);

    addArgV("sweepsInStage", std::vector<int>{5,5,1000});
    addArgV("minDim",std::vector<int>{10,10,10});
    addArgV("maxDim",std::vector<int>{30,50,200});
    addArgV("cutoff",std::vector<double>{1e-4,1e-6,1e-8});
    addArg("nSweepsSave",8);
    addArgS("name","someName");
    addArgS("startingPsi","none");

    calculate();

    return 0;
}
