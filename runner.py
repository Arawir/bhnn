import sys
import os
#################################
if "h" in sys.argv[1] or "H" in sys.argv[1]:
    print("python runner.py     N   M   g gnn  sweepsInStage    maxOcc    minDim    maxDim    cutoff     StartingPsi(if no none)  nameWhat" )
else:
    nameWhat = sys.argv[11]
    #################################

    command = './bin//job N{i}=' +sys.argv[1] \
            + ' M{i}='+sys.argv[2] \
            + ' g{d}='+sys.argv[3] \
            + ' gnn{d}='+sys.argv[4] \
            + ' sweepsInStage{i}='+sys.argv[5] \
            + ' maxOcc{i}='+sys.argv[6] \
            + ' minDim{i}='+sys.argv[7] \
            + ' maxDim{i}='+sys.argv[8] \
            + ' cutoff{d}='+sys.argv[9] \
            + ' startingPsi{s}='+sys.argv[10] \

    name = ""
    if "N" in nameWhat:
        name += "N"+str(sys.argv[1])+"_"
    if "M" in nameWhat:
        name += "M"+str(sys.argv[2])+"_"
    if "g" in nameWhat:
        name += "g"+str(sys.argv[3])+"_"
    if "gnn" in nameWhat:
        name += "gnn"+str(sys.argv[4])+"_"
    if "maxOcc" in nameWhat:
        name += "maxOcc"+str(sys.argv[6])+"_"
    if "minDim" in nameWhat:
        name += "minDim"+str(sys.argv[7])+"_"
    if "maxDim" in nameWhat:
        name += "maxDim"+str(sys.argv[8])+"_"
    if "cutoff" in nameWhat:
        name += "cutoff"+str(sys.argv[9])+"_"

    command += " name{s}="+name
    command += "  > "+name+".out"
          
    print("RUN COMMAND: " + command)
    os.system(command)
